/**
 * Created by saravana.sivakumar on 11/4/2015.
 */
angular
  .module("testingApp", ["angular.filter", "googlechart"])
  .controller("testingController", testingController);


function testingController($scope, $http, $filter) {
  var getJSONData = $http.get("../Data/SampleData.json");
  $scope.RXHDnumberOfTestCases = 0;
  $scope.RXHDFullyAutomated = 0;
  $scope.RXHDPartiallyAutomated = 0;

  $scope.RXCorenumberOfTestCases = 0;
  $scope.RXCoreFullyAutomated = 0;
  $scope.RXCorePartiallyAutomated = 0;

  $scope.GroupnumberOfTestCases = 0;
  $scope.GroupFullyAutomated = 0;
  $scope.GroupPartiallyAutomated = 0;

  $scope.DentalnumberOfTestCases = 0;
  $scope.DentalFullyAutomated = 0;
  $scope.DentalPartiallyAutomated = 0;

  $scope.OverallTestCase = false;
  $scope.OverallFullyAutomated = false;
  $scope.OverallPartiallyAutomated = false;

  $scope.RXHDApplication = true;
  $scope.RXCoreApplication = true;
  $scope.GroupApplication = true;
  $scope.DentalApplication = true;

  $scope.RXHDApplicationDD = true;
  $scope.TestCase = true;
  $scope.Fully = true;
  $scope.Partially = true;
  $scope.TestCase1 = true;
  $scope.Fully1 = true;
  $scope.Partially1 = true;
  $scope.TestCase2 = true;


  $scope.RXCoreApplicationDD = true;
  $scope.RXCoreTestCase = true;
  $scope.RXCoreFully = true;
  $scope.RXCorePartially = true;
  $scope.RXCoreTestCase1 = true;
  $scope.RXCoreFully1 = true;
  $scope.RXCorePartially1 = true;
  $scope.RXCoreTestCase2 = true;

  $scope.GroupApplicationDD = true;
  $scope.GroupTestCase = true;
  $scope.GroupFully = true;
  $scope.GroupPartially = true;
  $scope.GroupTestCase1 = true;
  $scope.GroupFully1 = true;
  $scope.GroupPartially1 = true;
  $scope.GroupTestCase2 = true;

  $scope.DentalApplicationDD = true;
  $scope.DentalTestCase = true;
  $scope.DentalFully = true;
  $scope.DentalPartially = true;
  $scope.DentalTestCase1 = true;
  $scope.DentalFully1 = true;
  $scope.DentalPartially1 = true;
  $scope.DentalTestCase2 = true;


  getJSONData.success(function (data) {
    $scope.groups = data;
console.log($scope.groups);
    $scope.ASGNames = [];
    $scope.RXHDApplicationNames = [];
    $scope.RXCoreApplicationNames = [];
    $scope.GroupApplicationNames = [];
    $scope.DentalApplicationNames = [];

    console.log(data.ASG[0].Group.groupName);

    for (var i = 0; i < data.ASG.length; i++) {
      console.log(data.ASG[i].Group.groupName);
      $scope.ASGNames.push(data.ASG[i].Group);
      //$scope.applicationNames.push(data.ASG[i].Group);

      if (data.ASG[i].Group.groupName == "Rx-HD") {

        $scope.RXHDnumberOfTestCases = $scope.RXHDnumberOfTestCases + data.ASG[i].Group.numberOfTestCases;
        $scope.RXHDFullyAutomated = $scope.RXHDFullyAutomated + data.ASG[i].Group.FullyAutomated;
        $scope.RXHDPartiallyAutomated = $scope.RXHDPartiallyAutomated + data.ASG[i].Group.PartiallyAutomated;
        $scope.RXHDApplicationNames.push(data.ASG[i].Group);
      }
      if (data.ASG[i].Group.groupName == "Rx-Core") {
        $scope.RXCorenumberOfTestCases = $scope.RXCorenumberOfTestCases + data.ASG[i].Group.numberOfTestCases;
        $scope.RXCoreFullyAutomated = $scope.RXCoreFullyAutomated + data.ASG[i].Group.FullyAutomated;
        $scope.RXCorePartiallyAutomated = $scope.RXCorePartiallyAutomated + data.ASG[i].Group.PartiallyAutomated;
        $scope.RXCoreApplicationNames.push(data.ASG[i].Group);
        console.log($scope.RXCorenumberOfTestCases + " " + $scope.RXCoreFullyAutomated + " " + $scope.RXCorePartiallyAutomated);

      }
      if (data.ASG[i].Group.groupName == "Group") {
        $scope.GroupnumberOfTestCases = $scope.GroupnumberOfTestCases + data.ASG[i].Group.numberOfTestCases;
        $scope.GroupFullyAutomated = $scope.GroupFullyAutomated + data.ASG[i].Group.FullyAutomated;
        $scope.GroupPartiallyAutomated = $scope.GroupPartiallyAutomated + data.ASG[i].Group.PartiallyAutomated;
        $scope.GroupApplicationNames.push(data.ASG[i].Group);
        console.log($scope.GroupnumberOfTestCases + " " + $scope.GroupFullyAutomated + " " + $scope.GroupPartiallyAutomated);
      }
      if (data.ASG[i].Group.groupName == "Dental") {
        $scope.DentalnumberOfTestCases = $scope.DentalnumberOfTestCases + data.ASG[i].Group.numberOfTestCases;
        $scope.DentalFullyAutomated = $scope.DentalFullyAutomated + data.ASG[i].Group.FullyAutomated;
        $scope.DentalPartiallyAutomated = $scope.DentalPartiallyAutomated + data.ASG[i].Group.PartiallyAutomated;
        $scope.DentalApplicationNames.push(data.ASG[i].Group);
        console.log($scope.DentalnumberOfTestCases + " " + $scope.DentalFullyAutomated + " " + $scope.DentalPartiallyAutomated);
      }
    }

    console.log("$scope.RXHDnumberOfTestCases--->"+$scope.RXHDnumberOfTestCases);
    console.log($scope.groups);
    console.log($scope.ASGNames);
    $scope.ASGNames = $filter('unique')($scope.ASGNames, 'groupName');
    console.log($scope.ASGNames);
    $scope.RXHDApplicationNames = $filter('unique')($scope.RXHDApplicationNames, 'Application');
    console.log($scope.RXHDApplicationNames);
    $scope.RXCoreApplicationNames = $filter('unique')($scope.RXCoreApplicationNames, 'Application');
    console.log($scope.RXCoreApplicationNames);
    $scope.GroupApplicationNames = $filter('unique')($scope.GroupApplicationNames, 'Application');
    console.log($scope.GroupApplicationNames);
    $scope.DentalApplicationNames = $filter('unique')($scope.DentalApplicationNames, 'Application');
    console.log($scope.DentalApplicationNames);

    $scope.RxHDChartObject = {};
    $scope.RxHDChartObject.type = "PieChart";
    var RxHDChartData = [];
    RxHDChartData.push(
      //{
      //  c: [
      //    {v: "Test Cases"}, {v: $scope.RXHDnumberOfTestCases}
      //  ]
      //},
      {
        c: [
          {v: "FullyAutomated"}, {v: $scope.RXHDFullyAutomated}
        ]
      },
      {
        c: [
          {v: "PartiallyAutomated"}, {v: $scope.RXHDPartiallyAutomated}
        ]
      }
    );
    console.log("----->>" + RxHDChartData);
    $scope.RxHDChartData = RxHDChartData;
    console.log("------>" + $scope.RxHDChartData);
    $scope.RxHDChartObject.data = {
      "cols": [
        {id: "n1", label: "NumberOfTestCases", type: "string"},
        {id: "f", label: "FullyAutomated", type: "number"},
        {id: "p", label: "PartiallyAutomated", type: "number"}
      ], "rows": $scope.RxHDChartData
    };
    console.log($scope.RxHDChartObject.data);
    $scope.RxHDChartObject.options = {
      'title': 'RX-HD'
    };

    $scope.RxCoreChartObject = {};
    $scope.RxCoreChartObject.type = "PieChart";
    var RxCoreChartData = [];
    RxCoreChartData.push(
      //{
      //  c: [
      //    {v: "Test Cases"}, {v: $scope.RXCorenumberOfTestCases}
      //  ]
      //},
      {
        c: [
          {v: "FullyAutomated"}, {v: $scope.RXCoreFullyAutomated}
        ]
      },
      {
        c: [
          {v: "PartiallyAutomated"}, {v: $scope.RXCorePartiallyAutomated}
        ]
      }
    );
    console.log("----->>" + RxCoreChartData);
    $scope.RxCoreChartData = RxCoreChartData;
    console.log("------>" + $scope.RxCoreChartData);
    $scope.RxCoreChartObject.data = {
      "cols": [
        {id: "n1", label: "NumberOfTestCases", type: "string"},

        //{id: "n", label: "NumberOfTestCases", type: "number"},
        {id: "f", label: "FullyAutomated", type: "number"},
        {id: "p", label: "PartiallyAutomated", type: "number"}
      ], "rows": $scope.RxCoreChartData
    };
    console.log($scope.RxCoreChartObject.data);
    $scope.RxCoreChartObject.options = {
      'title': 'RX-Core'
    };

    $scope.GroupChartObject = {};
    $scope.GroupChartObject.type = "PieChart";
    var GroupChartData = [];
    GroupChartData.push(
      //{
      //  c: [
      //    {v: "Test Cases"}, {v: $scope.GroupnumberOfTestCases}
      //  ]
      //},
      {
        c: [
          {v: "FullyAutomated"}, {v: $scope.GroupFullyAutomated}
        ]
      },
      {
        c: [
          {v: "PartiallyAutomated"}, {v: $scope.GroupPartiallyAutomated}
        ]
      }
    );
    console.log("----->>" + GroupChartData);
    $scope.GroupChartData = GroupChartData;
    console.log("------>" + $scope.GroupChartData);
    $scope.GroupChartObject.data = {
      "cols": [
        {id: "n1", label: "NumberOfTestCases", type: "string"},

        //{id: "n", label: "NumberOfTestCases", type: "number"},
        {id: "f", label: "FullyAutomated", type: "number"},
        {id: "p", label: "PartiallyAutomated", type: "number"}
      ], "rows": $scope.GroupChartData
    };
    console.log($scope.GroupChartObject.data);
    $scope.GroupChartObject.options = {
      'title': 'Group'
    };

    $scope.DentalChartObject = {};
    $scope.DentalChartObject.type = "PieChart";
    var DentalChartData = [];
    DentalChartData.push(
      //{
      //  c: [
      //    {v: "Test Cases"}, {v: $scope.DentalnumberOfTestCases}
      //  ]
      //},
      {
        c: [
          {v: "FullyAutomated"}, {v: $scope.DentalFullyAutomated}
        ]
      },
      {
        c: [
          {v: "PartiallyAutomated"}, {v: $scope.DentalPartiallyAutomated}
        ]
      }
    );
    console.log("----->>" + DentalChartData);
    $scope.DentalChartData = DentalChartData;
    console.log("------>" + $scope.DentalChartData);
    $scope.DentalChartObject.data = {
      "cols": [
        {id: "n1", label: "NumberOfTestCases", type: "string"},

        //{id: "n", label: "NumberOfTestCases", type: "number"},
        {id: "f", label: "FullyAutomated", type: "number"},
        {id: "p", label: "PartiallyAutomated", type: "number"}
      ], "rows": $scope.DentalChartData
    };
    console.log($scope.DentalChartObject.data);
    $scope.DentalChartObject.options = {
      'title': 'Dental'
    };


  });


  $scope.group1GridArea = "col-md-5";
  $scope.group2GridArea = "col-md-5";
  $scope.group3GridArea = "col-md-5";
  $scope.group4GridArea = "col-md-5";

  $scope.group1Btn = true;
  $scope.group2Btn = true;
  $scope.group3Btn = true;
  $scope.group4Btn = true;

  $scope.groupPosition = "absolute";
  $scope.groupLeft = "295px";
  $scope.groupWidth = "79%";


  $scope.showMoreGroup1 = function () {



    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;
    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.GroupApplicationDD = true;
    $scope.GroupTestCase = true;
    $scope.GroupFully = true;
    $scope.GroupPartially = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupFully1 = true;
    $scope.GroupPartially1 = true;
    $scope.GroupTestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;

    $scope.OverallTestCase = true;
    $scope.OverallFullyAutomated = true;
    $scope.OverallPartiallyAutomated = true;



    $scope.RXHDApplication = false;
    $scope.RXCoreApplication = true;
    $scope.GroupApplication = true;
    $scope.DentalApplication = true;


    $scope.group1GridArea = "col-md-12";
    $scope.group2GridArea = "col-md-4";
    $scope.group3GridArea = "col-md-4";
    $scope.group4GridArea = "col-md-4";
    $scope.group1Btn = false;

    $scope.group1width = '835px';
    $scope.group1height = '526px';
    $scope.group1IconPadding = '75px';
    $scope.group1IconPaddingLeft = '50px';
    $scope.group1paddingtop = '10px';

    $scope.group1Position = 'relative';
    $scope.group1left = '-135px';


    $scope.group2width = "190px";
    $scope.group2height = '165px';
    $scope.group2IconPadding = '75px';
    $scope.group2IconPaddingLeft = '50px';
    $scope.group2paddingleft = '45px';
    $scope.group2paddingtop = '75px';
    $scope.group2left = '-138px';

    $scope.group3width = "190px";
    $scope.group3height = '165px';
    $scope.group3IconPadding = '75px';
    $scope.group3IconPaddingLeft = '50px';
    $scope.group3Position = 'relative';
    $scope.group3Right = '-30px';
    $scope.group3Top = '15px';
    $scope.group3paddingleft = '45px';
    $scope.group3paddingtop = '75px';
    $scope.group3left = '-107px';

    $scope.group4width = "190px";
    $scope.group4height = '165px';
    $scope.group4IconPadding = '75px';
    $scope.group4IconPaddingLeft = '50px';
    $scope.group4Position = 'relative';
    $scope.group4Right = '-29px';
    $scope.group4Top = '31px';
    $scope.group4paddingleft = '45px';
    $scope.group4paddingtop = '75px';
    $scope.group4left = '-107px';


  };

  $scope.showMoreGroup2 = function () {

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.GroupApplicationDD = true;
    $scope.GroupTestCase = true;
    $scope.GroupFully = true;
    $scope.GroupPartially = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupFully1 = true;
    $scope.GroupPartially1 = true;
    $scope.GroupTestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;

    $scope.RXCoreApplication = false;
    $scope.RXHDApplication = true;
    $scope.GroupApplication = true;
    $scope.DentalApplication = true;




    $scope.OverallTestCase = true;
    $scope.OverallFullyAutomated = true;
    $scope.OverallPartiallyAutomated = true;

    $scope.group1GridArea = "col-md-4";
    $scope.group2GridArea = "col-md-12";
    $scope.group3GridArea = "col-md-4";
    $scope.group4GridArea = "col-md-4";
    $scope.group2Btn = false;

    $scope.group2width = '835px';
    $scope.group2height = '400px';
    $scope.group2IconPadding = '75px';
    $scope.group2IconPaddingLeft = '50px';
    $scope.group2paddingtop = '10px';
    $scope.group1Position = 'relative';
    $scope.group2left = '-143px';


    $scope.group1width = "200px";
    $scope.group1height = '400px';
    $scope.group1IconPadding = '75px';
    $scope.group1IconPaddingLeft = '50px';
    $scope.group1paddingleft = '60px';
    $scope.group1paddingtop = '175px';
    $scope.group1Position = 'relative';
    $scope.group1left = '-135px';

    $scope.group3width = "517px";
    $scope.group3height = '175px';
    $scope.group3IconPadding = '75px';
    $scope.group3IconPaddingLeft = '50px';
    $scope.group3Position = 'relative';
    $scope.group3Right = '0px';
    $scope.group3Top = '15px';
    $scope.group3paddingleft = '105px';
    $scope.group3paddingtop = '75px';
    $scope.group3left = '-135px';


    $scope.group4width = "517px";
    $scope.group4height = '175px';
    $scope.group4IconPadding = '75px';
    $scope.group4IconPaddingLeft = '50px';
    $scope.group4Position = 'relative';
    $scope.group4Right = '0px';
    $scope.group4Top = '15px';
    $scope.group4paddingleft = '130px';
    $scope.group4paddingtop = '75px';
    $scope.group4left = '-139px';
  };

  $scope.showMoreGroup3 = function () {

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;
    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;



    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;

    $scope.GroupApplication = false;
    $scope.RXHDApplication = true;
    $scope.RXCoreApplication = true;
    $scope.DentalApplication = true;



    $scope.OverallTestCase = true;
    $scope.OverallFullyAutomated = true;
    $scope.OverallPartiallyAutomated = true;

    $scope.group1GridArea = "col-md-4";
    $scope.group3GridArea = "col-md-12";
    $scope.group2GridArea = "col-md-4";
    $scope.group4GridArea = "col-md-4";
    $scope.group3Btn = false;

    $scope.group3width = '835px';
    $scope.group3height = '400px';
    $scope.group3IconPadding = '75px';
    $scope.group3IconPaddingLeft = '50px';
    $scope.group3paddingtop = '10px';
    $scope.group3Position = 'relative';
    $scope.group3left = '-135px';


    $scope.group1width = "517px";
    $scope.group1height = '165px';
    $scope.group1IconPadding = '75px';
    $scope.group1IconPaddingLeft = '50px';
    $scope.group1Position = 'relative';
    $scope.group1Top = '-10px';
    $scope.group1paddingleft = '120px';
    $scope.group1paddingtop = '70px';
    $scope.group1left = '-135px';

    $scope.group2width = "517px";
    $scope.group2height = '165px';
    $scope.group2IconPadding = '75px';
    $scope.group2IconPaddingLeft = '50px';
    $scope.group2Position = 'relative';
    $scope.group2Top = '-10px';
    $scope.group2paddingleft = '120px';
    $scope.group2paddingtop = '70px';
    $scope.group2left = '-144px';

    $scope.group4width = "200px";
    $scope.group4height = '400px';
    $scope.group4IconPadding = '75px';
    $scope.group4IconPaddingLeft = '50px';
    $scope.group4paddingleft = '70px';
    $scope.group4paddingtop = '185px';
    $scope.group4Position = 'relative';
    $scope.group4left = '-143px';
  };

  $scope.showMoreGroup4 = function () {

    $scope.DentalApplication = false;
    $scope.RXHDApplication = true;
    $scope.RXCoreApplication = true;
    $scope.GroupApplication = true;

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;
    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.GroupApplicationDD = true;
    $scope.GroupTestCase = true;
    $scope.GroupFully = true;
    $scope.GroupPartially = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupFully1 = true;
    $scope.GroupPartially1 = true;
    $scope.GroupTestCase2 = true;


    $scope.OverallTestCase = true;
    $scope.OverallFullyAutomated = true;
    $scope.OverallPartiallyAutomated = true;

    $scope.group1GridArea = "col-md-4";
    $scope.group4GridArea = "col-md-12";
    $scope.group3GridArea = "col-md-4";
    $scope.group2GridArea = "col-md-4";
    $scope.group4Btn = false;

    $scope.group4width = '98%';
    $scope.group4height = '400px';
    $scope.group4IconPadding = '75px';
    $scope.group4IconPaddingLeft = '50px';
    $scope.group4Position = 'relative';
    $scope.group4Right = '0px';
    $scope.group4Top = '0px';
    $scope.group4paddingtop = '10px';
    $scope.group4left = '-135px';

    $scope.group1width = "340px";
    $scope.group1height = '175px';
    $scope.group1IconPadding = '75px';
    $scope.group1IconPaddingLeft = '50px';
    $scope.group1paddingleft = '135px';
    $scope.group1paddingtop = '80px';
    $scope.group1left = '-135px';
    $scope.group1Top = '-10px';

    $scope.group3width = "340px";
    $scope.group3height = '175px';
    $scope.group3IconPadding = '75px';
    $scope.group3IconPaddingLeft = '50px';
    $scope.group3Position = 'relative';
    $scope.group3Right = '-29px';
    $scope.group3Top = '-10px';
    $scope.group3paddingleft = '135px';
    $scope.group3paddingtop = '80px';
    $scope.group3left = '-134px';


    $scope.group2width = "340px";
    $scope.group2height = '175px';
    $scope.group2IconPadding = '75px';
    $scope.group2IconPaddingLeft = '50px';
    $scope.group2Position = 'relative';
    $scope.group2Top = '10px';
    $scope.group2paddingleft = '135px';
    $scope.group2paddingtop = '80px';
    $scope.group2left = '-150px';
  };

  $scope.RXHDApplication1 = function(RXHDselectedItem){

    $scope.selectedApplication = RXHDselectedItem;
    var groups = $scope.groups;
    console.log($scope.groups);
    $scope.RXHDBusinessCapabilitys = [];
    $scope.RXHDApplication = true;
    $scope.RXHDApplicationDD = false;

    $scope.TestCase = false;
    $scope.TestCase1 = true;
    $scope.TestCase2 = true;
    $scope.Fully = false;
    $scope.Partially = false;

    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;
    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.GroupApplicationDD = true;
    $scope.GroupTestCase = true;
    $scope.GroupFully = true;
    $scope.GroupPartially = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupFully1 = true;
    $scope.GroupPartially1 = true;
    $scope.GroupTestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;



    $scope.ApplicationNumberOfTestCases = 0;
    $scope.ApplicationFullyAutomated = 0;
    $scope.ApplicationPartiallyAutomated = 0;



    for(var i=0;i<groups.ASG.length;i++){

      if(groups.ASG[i].Group.Application == RXHDselectedItem && groups.ASG[i].Group.groupName == "Rx-HD"){
        console.log(groups.ASG[i].Group.businessCapability);
        console.log(groups.ASG[i].Group.groupName);
        console.log(groups.ASG[i].Group.Application);
        console.log(RXHDselectedItem);
        $scope.RXHDBusinessCapabilitys.push(groups.ASG[i].Group.businessCapability);

        $scope.ApplicationNumberOfTestCases = $scope.ApplicationNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.ApplicationFullyAutomated = $scope.ApplicationFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.ApplicationPartiallyAutomated = $scope.ApplicationPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }

    console.log("$scope.RXHDBusinessCapability--->"+$scope.RXHDBusinessCapabilitys);
    $scope.RXHDBusinessCapabilitys = $filter('unique')($scope.RXHDBusinessCapabilitys);
    console.log($scope.RXHDBusinessCapabilitys);

  };

  $scope.RXHDBussiness = function(selectedItem){
    console.log(selectedItem);
    $scope.selectedBusiness = selectedItem;
    var groups = $scope.groups;
    $scope.RXHDFunctionalitys = [];

    //$scope.TestCase = true;
    //$scope.Fully = true;
    //$scope.Partially = true;
    $scope.TestCase = true;
    $scope.TestCase1 = false;
    $scope.TestCase2 = true;

    $scope.Fully1 = false;
    $scope.Partially1 = false;

    $scope.BussinessNumberOfTestCases = 0;
    $scope.BussinessFullyAutomated = 0;
    $scope.BussinessPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.businessCapability == selectedItem){
        $scope.RXHDFunctionalitys.push(groups.ASG[i].Group.functionality);

        $scope.BussinessNumberOfTestCases = $scope.BussinessNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.BussinessFullyAutomated = $scope.BussinessFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.BussinessPartiallyAutomated = $scope.BussinessPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }
    $scope.RXHDFunctionalitys = $filter('unique')($scope.RXHDFunctionalitys);
    console.log( $scope.RXHDFunctionalitys);

  };

  $scope.RXHDFunctionality = function(selectedItem){
    $scope.selectedFunctionality = selectedItem;
    $scope.TestCase = true;
    $scope.TestCase1 = true;
    $scope.TestCase2 = false;
    var groups = $scope.groups;

    $scope.FunctionalityNumberOfTestCases = 0;
    $scope.FunctionalityFullyAutomated = 0;
    $scope.FunctionalityPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.functionality == selectedItem){
        $scope.FunctionalityNumberOfTestCases = $scope.FunctionalityNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.FunctionalityFullyAutomated = $scope.FunctionalityFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.FunctionalityPartiallyAutomated = $scope.FunctionalityPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }


  };


  $scope.RXCoreApplication1 = function(RXCoreselectedItem){

    $scope.selectedRXCoreApplication = RXCoreselectedItem;
    var groups = $scope.groups;
    console.log($scope.groups);
    $scope.RXCoreBusinessCapabilitys = [];

    $scope.RXCoreApplication = true;
    $scope.RXCoreApplicationDD = false;

    $scope.RXCoreTestCase = false;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.RXCoreFully = false;
    $scope.RXCorePartially = false;

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.GroupApplicationDD = true;
    $scope.GroupTestCase = true;
    $scope.GroupFully = true;
    $scope.GroupPartially = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupFully1 = true;
    $scope.GroupPartially1 = true;
    $scope.GroupTestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;



    $scope.RXCoreApplicationNumberOfTestCases = 0;
    $scope.RXCoreApplicationFullyAutomated = 0;
    $scope.RXCoreApplicationPartiallyAutomated = 0;



    for(var i=0;i<groups.ASG.length;i++){

      if(groups.ASG[i].Group.Application == RXCoreselectedItem && groups.ASG[i].Group.groupName == "Rx-Core"){
        console.log(groups.ASG[i].Group.businessCapability);
        console.log(groups.ASG[i].Group.groupName);
        console.log(groups.ASG[i].Group.Application);
        console.log(RXCoreselectedItem);
        $scope.RXCoreBusinessCapabilitys.push(groups.ASG[i].Group.businessCapability);

        $scope.RXCoreApplicationNumberOfTestCases = $scope.RXCoreApplicationNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.RXCoreApplicationFullyAutomated = $scope.RXCoreApplicationFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.RXCoreApplicationPartiallyAutomated = $scope.RXCoreApplicationPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }

    console.log("$scope.RXCoreBusinessCapability--->"+$scope.RXCoreBusinessCapabilitys);
    $scope.RXCoreBusinessCapabilitys = $filter('unique')($scope.RXCoreBusinessCapabilitys);
    console.log($scope.RXCoreBusinessCapabilitys);

  };

  $scope.RXCoreBussiness = function(selectedItem){
    console.log(selectedItem);
    $scope.selectedRXCoreBusiness = selectedItem;
    var groups = $scope.groups;
    $scope.RXCoreFunctionalitys = [];

    $scope.RXCoreTestCase = true;
    $scope.RXCoreTestCase1 = false;
    $scope.RXCoreTestCase2 = true;

    $scope.Fully1 = false;
    $scope.Partially1 = false;

    $scope.RXCoreBussinessNumberOfTestCases = 0;
    $scope.RXCoreBussinessFullyAutomated = 0;
    $scope.RXCoreBussinessPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.businessCapability == selectedItem){
        $scope.RXCoreFunctionalitys.push(groups.ASG[i].Group.functionality);

        $scope.RXCoreBussinessNumberOfTestCases = $scope.RXCoreBussinessNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.RXCoreBussinessFullyAutomated = $scope.RXCoreBussinessFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.RXCoreBussinessPartiallyAutomated = $scope.RXCoreBussinessPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }
    $scope.RXCoreFunctionalitys = $filter('unique')($scope.RXCoreFunctionalitys);
    console.log( $scope.RXCoreFunctionalitys);

  };

  $scope.RXCoreFunctionality = function(selectedItem){
    $scope.selectedRXCoreFunctionality = selectedItem;

    $scope.RXCoreTestCase = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreTestCase2 = false;

    var groups = $scope.groups;

    $scope.RXCoreFunctionalityNumberOfTestCases = 0;
    $scope.RXCoreFunctionalityFullyAutomated = 0;
    $scope.RXCoreFunctionalityPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.functionality == selectedItem){
        $scope.RXCoreFunctionalityNumberOfTestCases = $scope.RXCoreFunctionalityNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.RXCoreFunctionalityFullyAutomated = $scope.RXCoreFunctionalityFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.RXCoreFunctionalityPartiallyAutomated = $scope.RXCoreFunctionalityPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }


  };


  $scope.GroupApplication1 = function(GroupselectedItem){



    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;

    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.DentalApplicationDD = true;
    $scope.DentalTestCase = true;
    $scope.DentalFully = true;
    $scope.DentalPartially = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalFully1 = true;
    $scope.DentalPartially1 = true;
    $scope.DentalTestCase2 = true;

    $scope.selectedGroupApplication = GroupselectedItem;

    var groups = $scope.groups;
    console.log("Groups--->"+$scope.groups);
    $scope.GroupBusinessCapabilitys = [];
    $scope.GroupApplication = true;
    $scope.GroupApplicationDD = false;

    $scope.GroupTestCase = false;
    $scope.GroupTestCase1 = true;
    $scope.GroupTestCase2 = true;
    $scope.GroupFully = false;
    $scope.GroupPartially = false;

    $scope.GroupApplicationNumberOfTestCases = 0;
    $scope.GroupApplicationFullyAutomated = 0;
    $scope.GroupApplicationPartiallyAutomated = 0;



    for(var i=0;i<groups.ASG.length;i++){

      if(groups.ASG[i].Group.Application == GroupselectedItem && groups.ASG[i].Group.groupName == "Group"){
        console.log(groups.ASG[i].Group.businessCapability);
        console.log(groups.ASG[i].Group.groupName);
        console.log(groups.ASG[i].Group.Application);
        console.log(GroupselectedItem);
        $scope.GroupBusinessCapabilitys.push(groups.ASG[i].Group.businessCapability);

        $scope.GroupApplicationNumberOfTestCases = $scope.GroupApplicationNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.GroupApplicationFullyAutomated = $scope.GroupApplicationFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.GroupApplicationPartiallyAutomated = $scope.GroupApplicationPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }

    console.log("$scope.GroupBusinessCapability--->"+$scope.GroupBusinessCapabilitys);
    $scope.GroupBusinessCapabilitys = $filter('unique')($scope.GroupBusinessCapabilitys);
    console.log($scope.GroupBusinessCapabilitys);

  };

  $scope.GroupBussiness = function(selectedItem){
    console.log(selectedItem);
    $scope.selectedGroupBusiness = selectedItem;
    var groups = $scope.groups;
    $scope.GroupFunctionalitys = [];

    $scope.GroupTestCase = true;
    $scope.GroupTestCase1 = false;
    $scope.GroupTestCase2 = true;

    $scope.Fully1 = false;
    $scope.Partially1 = false;

    $scope.GroupBussinessNumberOfTestCases = 0;
    $scope.GroupBussinessFullyAutomated = 0;
    $scope.GroupBussinessPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.businessCapability == selectedItem){
        $scope.GroupFunctionalitys.push(groups.ASG[i].Group.functionality);

        $scope.GroupBussinessNumberOfTestCases = $scope.GroupBussinessNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.GroupBussinessFullyAutomated = $scope.GroupBussinessFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.GroupBussinessPartiallyAutomated = $scope.GroupBussinessPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }
    $scope.GroupFunctionalitys = $filter('unique')($scope.GroupFunctionalitys);
    console.log( $scope.GroupFunctionalitys);

  };

  $scope.GroupFunctionality = function(selectedItem){
    $scope.selectedGroupFunctionality = selectedItem;

    $scope.GroupTestCase = true;
    $scope.GroupTestCase1 = true;
    $scope.GroupTestCase2 = false;

    var groups = $scope.groups;

    $scope.GroupFunctionalityNumberOfTestCases = 0;
    $scope.GroupFunctionalityFullyAutomated = 0;
    $scope.GroupFunctionalityPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.functionality == selectedItem){
        $scope.GroupFunctionalityNumberOfTestCases = $scope.GroupFunctionalityNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.GroupFunctionalityFullyAutomated = $scope.GroupFunctionalityFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.GroupFunctionalityPartiallyAutomated = $scope.GroupFunctionalityPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }


  };



  $scope.DentalApplication1 = function(DentalselectedItem){

    $scope.RXCoreApplicationDD = true;
    $scope.RXCoreTestCase = true;
    $scope.RXCoreFully = true;
    $scope.RXCorePartially = true;
    $scope.RXCoreTestCase1 = true;
    $scope.RXCoreFully1 = true;
    $scope.RXCorePartially1 = true;
    $scope.RXCoreTestCase2 = true;

    $scope.RXHDApplicationDD = true;
    $scope.TestCase = true;
    $scope.Fully = true;
    $scope.Partially = true;
    $scope.TestCase1 = true;
    $scope.Fully1 = true;
    $scope.Partially1 = true;
    $scope.TestCase2 = true;

    $scope.selectedDentalApplication = DentalselectedItem;

    var groups = $scope.groups;
    console.log($scope.groups);
    $scope.DentalBusinessCapabilitys = [];
    $scope.DentalApplication = true;
    $scope.DentalApplicationDD = false;

    $scope.DentalTestCase = false;
    $scope.DentalTestCase1 = true;
    $scope.DentalTestCase2 = true;
    $scope.DentalFully = false;
    $scope.DentalPartially = false;

    $scope.DentalApplicationNumberOfTestCases = 0;
    $scope.DentalApplicationFullyAutomated = 0;
    $scope.DentalApplicationPartiallyAutomated = 0;



    for(var i=0;i<groups.ASG.length;i++){

      if(groups.ASG[i].Group.Application == DentalselectedItem && groups.ASG[i].Group.groupName == "Dental"){
        console.log(groups.ASG[i].Group.businessCapability);
        console.log(groups.ASG[i].Group.DentalName);
        console.log(groups.ASG[i].Group.Application);
        console.log(DentalselectedItem);
        $scope.DentalBusinessCapabilitys.push(groups.ASG[i].Group.businessCapability);

        $scope.DentalApplicationNumberOfTestCases = $scope.DentalApplicationNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.DentalApplicationFullyAutomated = $scope.DentalApplicationFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.DentalApplicationPartiallyAutomated = $scope.DentalApplicationPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }

    console.log("$scope.DentalBusinessCapability--->"+$scope.DentalBusinessCapabilitys);
    $scope.DentalBusinessCapabilitys = $filter('unique')($scope.DentalBusinessCapabilitys);
    console.log($scope.DentalBusinessCapabilitys);

  };

  $scope.DentalBussiness = function(selectedItem){
    console.log(selectedItem);
    $scope.selectedDentalBusiness = selectedItem;
    var groups = $scope.groups;
    $scope.DentalFunctionalitys = [];

    $scope.DentalTestCase = true;
    $scope.DentalTestCase1 = false;
    $scope.DentalTestCase2 = true;

    $scope.Fully1 = false;
    $scope.Partially1 = false;

    $scope.DentalBussinessNumberOfTestCases = 0;
    $scope.DentalBussinessFullyAutomated = 0;
    $scope.DentalBussinessPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.businessCapability == selectedItem){
        $scope.DentalFunctionalitys.push(groups.ASG[i].Group.functionality);

        $scope.DentalBussinessNumberOfTestCases = $scope.DentalBussinessNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.DentalBussinessFullyAutomated = $scope.DentalBussinessFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.DentalBussinessPartiallyAutomated = $scope.DentalBussinessPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }
    $scope.DentalFunctionalitys = $filter('unique')($scope.DentalFunctionalitys);
    console.log( $scope.DentalFunctionalitys);

  };

  $scope.DentalFunctionality = function(selectedItem){
    $scope.selectedDentalFunctionality = selectedItem;

    $scope.DentalTestCase = true;
    $scope.DentalTestCase1 = true;
    $scope.DentalTestCase2 = false;

    var groups = $scope.groups;

    $scope.DentalFunctionalityNumberOfTestCases = 0;
    $scope.DentalFunctionalityFullyAutomated = 0;
    $scope.DentalFunctionalityPartiallyAutomated = 0;

    for(var i=0;i<groups.ASG.length;i++){
      if(groups.ASG[i].Group.functionality == selectedItem){
        $scope.DentalFunctionalityNumberOfTestCases = $scope.DentalFunctionalityNumberOfTestCases + groups.ASG[i].Group.numberOfTestCases;
        $scope.DentalFunctionalityFullyAutomated = $scope.DentalFunctionalityFullyAutomated + groups.ASG[i].Group.FullyAutomated;
        $scope.DentalFunctionalityPartiallyAutomated = $scope.DentalFunctionalityPartiallyAutomated + groups.ASG[i].Group.PartiallyAutomated;

      }
    }


  };



}
